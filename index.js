const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const PORT = 5000;
const cors = require('cors');
const routes = require('./settings/routes');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());

routes(app);

app.listen(PORT, error => {
   error ? console.log('Something wrong, ' + error) : console.log('Server is listening on port ' + PORT);
});

