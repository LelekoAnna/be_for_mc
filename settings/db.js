const mysql = require('mysql');

const connection = mysql.createConnection({
    host: 'localhost',
    user: 'lann',
    password: '123',
    database: 'mortgagedb'
});

connection.connect(error => {
    return error ? console.log('error connection db') : console.log('connection is successful');
});

module.exports = connection;
