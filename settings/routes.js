'use strict';

module.exports = (app) => {
    const banksController = require('./../Controller/banksController');

    app.route('/banks').get(banksController.banks);
    app.route('/banks/:id').get(banksController.bank);
    app.route('/banks').post(banksController.add);
    app.route('/banks/:id').delete(banksController.delete);
};
