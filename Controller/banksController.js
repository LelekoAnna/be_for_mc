'use strict';

const response = require('./../response');
const db = require('./../settings/db');

exports.banks = (req, res) => {
    db.query("SELECT * FROM banks", (error, rows, fields) => {
        error ? console.log(error) : response.status(rows, res)
    });
};

exports.bank = (req, res) => {
    const id = req.params.id;

    const sql = "SELECT * FROM banks WHERE id = ?";
    db.query(sql, id, (error, rows, fields) => {
        error ? console.log(error) : response.status(rows, res)
    });
};

exports.add = (req, res) => {
    const bankName = req.body.bankName;
    const interestRate = req.body.interestRate;
    const maxLoan = req.body.maxLoan;
    const minDownPayment = req.body.minDownPayment;
    const loanTerm = req.body.loanTerm;

    const sql =
        "INSERT INTO banks (bankName, interestRate, maxLoan, minDownPayment, loanTerm) VALUES(?, ?, ?, ? ,?)";
    db.query(sql, [bankName, interestRate, maxLoan, minDownPayment, loanTerm], (error, results) => {
        error ? console.log(error) : response.status(results, res);
    });
};

exports.delete = (req, res) => {
    const id = req.params.id;

    const sql = "DELETE FROM banks WHERE id = ?";
    db.query(sql, id, (error, results) => {
        error ? console.log(error) : response.status(results, res);
    });
};
